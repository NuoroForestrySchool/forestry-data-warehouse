--
-- File generated with SQLiteStudio v3.2.1 on mer feb 3 23:33:23 2021
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: Campagne
DROP TABLE IF EXISTS Campagne;

CREATE TABLE Campagne (
    ID_Campagna        TEXT NOT NULL,
    ComplessoForestale TEXT NOT NULL,
    SitoDiOsservazione TEXT NOT NULL,
    CONSTRAINT pk_campagne_id_campagna PRIMARY KEY (
        ID_Campagna
    ),
    FOREIGN KEY (
        ID_Campagna
    )
    REFERENCES Rilievi (ID_Campagna) 
);


-- Table: ClassiAltezza
DROP TABLE IF EXISTS ClassiAltezza;

CREATE TABLE ClassiAltezza (
    cls_h          CHAR PRIMARY KEY,
    classe_altezza TEXT
);


-- Table: Corteccia
DROP TABLE IF EXISTS Corteccia;

CREATE TABLE Corteccia (
    KeyID       VARCHAR (10),
    cls_h       CHAR,
    over_under  CHAR (1),
    large_small CHAR (1),
    diam        INTEGER,
    PRIMARY KEY (
        KeyID,
        cls_h,
        over_under,
        large_small
    ),
    FOREIGN KEY (
        KeyID
    )
    REFERENCES FustiCampioneEtAdF (KeyID),
    FOREIGN KEY (
        cls_h
    )
    REFERENCES ClassiAltezza (cls_h) 
);


-- Table: FustiCampioneEtAdF
DROP TABLE IF EXISTS FustiCampioneEtAdF;

CREATE TABLE FustiCampioneEtAdF (
    KeyID              VARCHAR (10)   NOT NULL,
    ID_Rilievo         VARCHAR (10)   NOT NULL,
    ID_fustoCampione   TEXT           NOT NULL,
    ID_gradone         TEXT           NOT NULL,
    Prog_gradone       TEXT,
    SpecieEPPOcod      TEXT           NOT NULL,
    d_130              INTEGER,
    h_ipso             NUMERIC (4, 2),
    lunghezzaAbbattuto NUMERIC (4, 2),
    pesoFrescoRamaglia NUMERIC (5, 2),
    pesoRotelle        NUMERIC (5, 2),
    CONSTRAINT pk_fusticampioneetadf_keyid PRIMARY KEY (
        KeyID
    ),
    FOREIGN KEY (
        ID_Rilievo
    )
    REFERENCES Rilievi (ID_Rilievo),
    FOREIGN KEY (
        SpecieEPPOcod
    )
    REFERENCES Specie (SpecieEPPOcod) 
);


-- Table: FustiSecondari
DROP TABLE IF EXISTS FustiSecondari;

CREATE TABLE FustiSecondari (
    KeyID     VARCHAR (10)   NOT NULL,
    ID_asta   VARCHAR (2)    NOT NULL,
    distSuolo NUMERIC (5, 2),
    CONSTRAINT _2 PRIMARY KEY (
        KeyID,
        ID_asta
    ),
    FOREIGN KEY (
        KeyID
    )
    REFERENCES FustiCampioneEtAdF
);


-- Table: NOTE
DROP TABLE IF EXISTS NOTE;

CREATE TABLE NOTE (
    TIPO CHAR,
    DATA DATE,
    NOTA VARCHAR
);


-- Table: Palchi
DROP TABLE IF EXISTS Palchi;

CREATE TABLE Palchi (
    KeyID          VARCHAR (10)   NOT NULL,
    ID_palco       INTEGER        NOT NULL,
    stato          CHAR (1),
    numeroRami     INTEGER,
    diamRamoGrosso NUMERIC (4, 2),
    distSuolo      NUMERIC (5, 2),
    ID_asta        CHAR (1),
    CONSTRAINT pk_palchi_id_fustocampione PRIMARY KEY (
        KeyID,
        ID_palco
    ),
    FOREIGN KEY (
        KeyID
    )
    REFERENCES FustiCampioneEtAdF
);


-- Table: ProfiliFustiPrincipali
DROP TABLE IF EXISTS ProfiliFustiPrincipali;

CREATE TABLE ProfiliFustiPrincipali (
    KeyID     VARCHAR (10)   NOT NULL,
    distSuolo NUMERIC (5, 2) NOT NULL,
    d_sez     NUMERIC (5, 2),
    CONSTRAINT _4 PRIMARY KEY (
        KeyID,
        distSuolo
    ),
    FOREIGN KEY (
        KeyID
    )
    REFERENCES FustiCampioneEtAdF
);


-- Table: ProfiliFustiSecondari
DROP TABLE IF EXISTS ProfiliFustiSecondari;

CREATE TABLE ProfiliFustiSecondari (
    KeyID                VARCHAR (10)   NOT NULL,
    ID_asta              VARCHAR (2)    NOT NULL,
    distanzaBiforcazione NUMERIC (4, 2) NOT NULL,
    diametro             NUMERIC (4, 2),
    CONSTRAINT pk_interpalchi_fustisecondari_id_fustocampione PRIMARY KEY (
        KeyID,
        ID_asta,
        distanzaBiforcazione
    ),
    FOREIGN KEY (
        KeyID,
        ID_asta
    )
    REFERENCES FustiSecondari
);


-- Table: Rilevatori
DROP TABLE IF EXISTS Rilevatori;

CREATE TABLE Rilevatori (
    ID_Rilevatore VARCHAR (10) NOT NULL,
    Cognome       TEXT,
    Nome          TEXT,
    Affiliazione  TEXT,
    CONSTRAINT pk_Rilevatori UNIQUE (
        ID_Rilevatore
    ),
    CONSTRAINT Pk_Rilevatori_ID_Rilevatore PRIMARY KEY (
        ID_Rilevatore
    )
);


-- Table: Rilievi
DROP TABLE IF EXISTS Rilievi;

CREATE TABLE Rilievi (
    ID_Rilievo       VARCHAR (10) NOT NULL,
    ID_Campagna      TEXT         NOT NULL,
    dataRilievo_INT  INTEGER      NOT NULL,
    dataRilievo_DATE DATE,
    CONSTRAINT _5 PRIMARY KEY (
        ID_Rilievo
    ),
    CONSTRAINT sqlite_autoindex_Rilievi_1 UNIQUE (
        ID_Campagna,
        dataRilievo_INT
    )
);


-- Table: Rotelle
DROP TABLE IF EXISTS Rotelle;

CREATE TABLE Rotelle (
    KeyID     VARCHAR (10)   NOT NULL,
    cls_h     CHAR           NOT NULL
                             REFERENCES ClassiAltezza (cls_h),
    distSuolo NUMERIC (5, 2),
    CONSTRAINT _1 PRIMARY KEY (
        KeyID,
        cls_h
    ),
    FOREIGN KEY (
        KeyID
    )
    REFERENCES FustiCampioneEtAdF
);


-- Table: Specie
DROP TABLE IF EXISTS Specie;

CREATE TABLE Specie (
    SpecieEPPOcod   TEXT NOT NULL,
    nomeScientifico TEXT NOT NULL,
    nomeItaliano    TEXT NOT NULL,
    species         TEXT NOT NULL,
    CONSTRAINT pk_specie_id_specie PRIMARY KEY (
        SpecieEPPOcod
    )
);


-- Table: Squadre
DROP TABLE IF EXISTS Squadre;

CREATE TABLE Squadre (
    ID_Rilievo    VARCHAR (10) NOT NULL,
    ID_Rilevatore VARCHAR (10) NOT NULL,
    CONSTRAINT _3 PRIMARY KEY (
        ID_Rilievo,
        ID_Rilevatore
    ),
    FOREIGN KEY (
        ID_Rilevatore
    )
    REFERENCES Rilevatori (ID_Rilevatore),
    FOREIGN KEY (
        ID_Rilievo
    )
    REFERENCES Rilievi (ID_Rilievo) 
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
