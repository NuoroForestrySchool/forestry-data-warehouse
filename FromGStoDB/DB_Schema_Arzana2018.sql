CREATE TABLE Rilevatori ( 
	ID_Rilevatore        varchar(10) NOT NULL  ,
	Cognome              text   ,
	Nome                 text   ,
	Affiliazione         text   ,
	CONSTRAINT pk_Rilevatori UNIQUE ( ID_Rilevatore ) ,
	CONSTRAINT Pk_Rilevatori_ID_Rilevatore PRIMARY KEY ( ID_Rilevatore )
 );

CREATE TABLE Rilievi ( 
	ID_Rilievo           varchar(10) NOT NULL  ,
	ID_Campagna          text NOT NULL  ,
	dataRilievo          date NOT NULL  ,
	CONSTRAINT _5 PRIMARY KEY ( ID_Rilievo ),
	CONSTRAINT sqlite_autoindex_Rilievi_1 UNIQUE ( ID_Campagna, dataRilievo ) 
 );

CREATE TABLE Specie ( 
	ID_Specie            text NOT NULL  ,
	nomeScientifico      text NOT NULL  ,
	nomeItaliano         text NOT NULL  ,
	species              text NOT NULL  ,
	CONSTRAINT pk_specie_id_specie PRIMARY KEY ( ID_Specie )
 );

CREATE TABLE Squadre ( 
	ID_Rilievo           varchar(10) NOT NULL  ,
	ID_Rilevatore        varchar(10) NOT NULL  ,
	CONSTRAINT _3 PRIMARY KEY ( ID_Rilievo, ID_Rilevatore ),
	FOREIGN KEY ( ID_Rilevatore ) REFERENCES Rilevatori( ID_Rilevatore )  ,
	FOREIGN KEY ( ID_Rilievo ) REFERENCES Rilievi( ID_Rilievo )  
 );

CREATE TABLE Campagne ( 
	ID_Campagna          text NOT NULL  ,
	ComplessoForestale   text NOT NULL  ,
	SitoDiOsservazione   text NOT NULL  ,
	CONSTRAINT pk_campagne_id_campagna PRIMARY KEY ( ID_Campagna ),
	FOREIGN KEY ( ID_Campagna ) REFERENCES Rilievi( ID_Campagna )  
 );

CREATE TABLE FustiCampioneEtAdF ( 
	KeyID                varchar(10) NOT NULL  ,
	ID_Rilievo           varchar(10) NOT NULL  ,
	ID_fustoCampione     text NOT NULL  ,
	ID_gradone           text NOT NULL  ,
	Prog_gradone         text   ,
	ID_specie            text NOT NULL  ,
	d_130                integer   ,
	h_ipso               numeric(4,2)   ,
	lunghezzaAbbattuto   numeric(4,2)   ,
	pesoFrescoRamaglia   numeric(5,2)   ,
	pesoRotelle          numeric(5,2)   ,
	CONSTRAINT pk_fusticampioneetadf_keyid PRIMARY KEY ( KeyID ),
	FOREIGN KEY ( ID_Rilievo ) REFERENCES Rilievi( ID_Rilievo )  ,
	FOREIGN KEY ( ID_specie ) REFERENCES Specie( ID_Specie )  
 );

CREATE INDEX Idx_FustiCampioneEtAdF_ID_specie ON FustiCampioneEtAdF ( ID_specie );

CREATE INDEX Idx_FustiCampioneEtAdF_ID_Rilievo ON FustiCampioneEtAdF ( ID_Rilievo );

CREATE TABLE FustiSecondari ( 
	KeyID                varchar(10) NOT NULL  ,
	ID_asta              varchar(2) NOT NULL  ,
	distSuolo            numeric(5,2)   ,
	CONSTRAINT _2 PRIMARY KEY ( KeyID, ID_asta ),
	FOREIGN KEY ( KeyID ) REFERENCES FustiCampioneEtAdF( ID_fustoCampione )  
 );

CREATE TABLE Palchi ( 
	KeyID                varchar(10) NOT NULL  ,
	ID_palco             integer NOT NULL  ,
	stato                char(1)   ,
	numeroRami           integer   ,
	diamRamoGrosso       numeric(4,2)   ,
	distSuolo            numeric(5,2)   ,
	ID_asta              char(1)   ,
	CONSTRAINT pk_palchi_id_fustocampione PRIMARY KEY ( KeyID, ID_palco ),
	FOREIGN KEY ( KeyID ) REFERENCES FustiCampioneEtAdF( ID_fustoCampione )  
 );

CREATE INDEX Idx_palchi_ID_fustoCampione ON Palchi ( KeyID );

CREATE TABLE ProfiliFustiPrincipali ( 
	KeyID                varchar(10) NOT NULL  ,
	distSuolo            numeric(5,2) NOT NULL  ,
	d_sez                numeric(5,2)   ,
	CONSTRAINT _4 PRIMARY KEY ( KeyID, distSuolo ),
	FOREIGN KEY ( KeyID ) REFERENCES FustiCampioneEtAdF( KeyID )  
 );

CREATE INDEX Idx_ProfiliFustiPrincipali_KeyID ON ProfiliFustiPrincipali ( KeyID );

CREATE TABLE ProfiliFustiSecondari ( 
	KeyID                varchar(10) NOT NULL  ,
	ID_asta              varchar(2) NOT NULL  ,
	distanzaBiforcazione numeric(4,2) NOT NULL  ,
	diametro             numeric(4,2)   ,
	CONSTRAINT pk_interpalchi_fustisecondari_id_fustocampione PRIMARY KEY ( KeyID, ID_asta, distanzaBiforcazione ),
	FOREIGN KEY ( KeyID, ID_asta ) REFERENCES FustiSecondari( KeyID, ID_asta )  
 );

CREATE TABLE Rotelle ( 
	KeyID                varchar(10) NOT NULL  ,
	cls_h                integer NOT NULL  ,
	distSuolo            numeric(5,2)   ,
	CONSTRAINT _1 PRIMARY KEY ( KeyID, cls_h ),
	FOREIGN KEY ( KeyID ) REFERENCES FustiCampioneEtAdF( KeyID )  
 );

CREATE INDEX Idx_Rotelle_KeyID ON Rotelle ( KeyID );

CREATE VIEW FustiCampioneEtAdF_view AS SELECT * FROM FustiCampioneEtAdF JOIN Specie USING (ID_Specie);;

