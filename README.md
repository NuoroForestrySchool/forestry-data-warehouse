# Forestry Data Warehouse  
Collecting and organizing forest mensuration data  

## Process

### 2018 'Canali Esca' (Arzana, Nuoro, Sardegna, Italy)
Pinus radiata and pinaster plantation  

#### 1 - DB initialization
'FromGStoDB': Importing stem profiles and stem sections (positions only) for growth analysis 

#### 2 - DB schema adjustment
Rename ID_Specie  to SpecieEPPOcod
Add DataRilievo_DATE
Add Table Corteccia
* details in: diff_inDB_2018-2021.log (in )

#### 3 - DB append bark thikness measures
'AddBark' from a subset of stems
