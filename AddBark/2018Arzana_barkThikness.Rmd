---
title: "Adding bark measurements to the DataBase"
author:
- name: Roberto Scotti
  affiliation: NuoroForestrySchool
- name: Matteo Mura
  affiliation: NuoroForestrySchool
date: "`r format(Sys.time(), '%B %d, %Y')`"
output: pdf_document
abstract: |
  A stratified random sample of stems has been selected, during the year 2018, within the radiata and maritime pines plantation located in 'Canali Esca' (Arzana, Nuoro, Sardegna), to analyse stand growth by 'stem analysis'.  

  To enable also accurate mesurements of bark thikness, for each stem section a perpendicular photo has been taken,  after having placed a tree caliper crossing section pith on the upper side.  

  A sample of the available images has been selected in order to test the the modelling problem. For each species, stems are divided in three large dbh classes, three stems have been randomly selected in each class and the sections 1, 2 and 4 (at heights respectively circa 50 cm, brest height and half of total height) have been considered.  

  Bark measurements have been carried out analysing the photos with "LignoVision"" ((R) RinnTech).  

  Using the values that are visible on the caliper to define a reference measure, over-bark and under-bark diameter mesurements have been taken following two directions, the largest (L) and the narrowest (S) diameters.  Diamiter measures are in millimiters [mm].
  

  Repo: https://gitlab.com/NuoroForestrySchool/forestry-data-warehouse  
  DB schema modifications - two new tables (Corteccia and NOTE)  
  schema 'diff' in 'diff_2018-2021.png'  
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse)
```

# Raw data storage  

Images and "*.fh" files produced by LignoVision (R) are stored in Google Drive folders below the "BarkMeas" folder 
```{r googledrive}
library(googledrive)
drive_auth(path = "/node/local/Rwork/Marongiu_Arzana/Forest-Management-Tools_pinaster-and-radiata-pine_Arzana/.Rproj.user/cd26ed5dc626f11802a652e81d02762e_scotti@uniss.it")
fl <- drive_ls(path = as_id("1hxsY46VYmijJv90A0CR_eldKtyzCCuwq"),
               recursive = T,
               pattern = 'BarkMeas.fh'
               )
```

# Structure of the '.fh' files  
  
HEADER:  
DateEnd=4  
KeyCode=R77_1_Bark_meas_RW  
Unit=mm  
Length=5  
Location=Arzana - Canali Esca  
Species=PIRA    Pinus radiata  
Project=Forest management tools  
DataType=Ringwidth  
SeriesType=Single curve  
DATA:Tree  
520;;  = MISURA DI CALIBRAZIONE DELLO STRUMENTO SULL'ASTA GRADUATA DEL CAVALLETTO  
566;;  = DIAMETRO MAGGIORE SOPRA CORTECCIA  
483;;  = DIAMETRO MAGGIORE SOTTO CORTECCIA  
513;;  = DIAMETRO MINORE SOPRA CORTECCIA  
433;;  = DIAMETRO MINORE SOTTO CORTECCIA  

```{r read_fh-files}
read_bark_fh <- function(fid) {
  temp <- tempfile(fileext = ".fh")
  dl <- drive_download(as_id(fid), path = temp, overwrite = TRUE)
  if (str_split(dl$name, "_Bark", n = 2, simplify = T)[1] !=
      (str_split(
        read_lines(dl$local_path, skip = 2, n_max = 1),
        "_Bark", n = 2, simplify = T )[1] %>%
      str_sub(9) -> kc)
      )  cat("WARNING: KeyCode '", kc, "' and file name are not corresponding!\n")
  tibble(
    ID_fustoCampione = substr(dl$name, 2, 3),
    cls_h = str_split(dl$name, "_", simplify = T)[, 2],
    large_small = c("L", "L", "S", "S"),
    over_under = c("O", "U", "O", "U"),
    diam = str_split(
      read_lines(dl$local_path, skip = 12, n_max = 4),
      ";",
      n = 2,
      simplify = T
    )[, 1]
  )
}
# File downloaded:
#   * R72_4_BarkMeas.fh
# WARNING: KeyCode ' R47_4 ' and file name are not corresponding!
####  [rsMEMO: actually file name has been edited by me supposing that the containg folder "R72" is rigth!]

s <- Sys.time()
bark_measures <- map_dfr(fl$id, read_bark_fh)
print(paste("Start:", s, "- time diff [min]:", difftime(Sys.time(), s)/60))
save(bark_measures, file = "bark_measures.Rdata")

```

# Bark data verification

```{r verify}
load("bark_measures.Rdata")

source("../DataBase/Open_DB_fun.R")
fm_DB <- "2018Arzana_SampleStems_v2021.sqlite"
con <- Open_DB(fm_DB)

for(t in c("FustiCampioneEtAdF", "Rotelle", "Corteccia")) { 
  assign(t, as_tibble(tbl(con, t))) }
dbDisconnect(con)

# Retrieve KeyID, the base ID in the DB  
Corteccia <- FustiCampioneEtAdF %>% 
  select(KeyID, ID_fustoCampione) %>% 
  right_join((bark_measures %>% 
                mutate(ID_fustoCampione = as.character( as.numeric(ID_fustoCampione)))
             ), by = "ID_fustoCampione") %>%    
  mutate(diam = as.numeric(diam)) %>% 
  select(-ID_fustoCampione) %>% 
  arrange(KeyID, cls_h, large_small, over_under)

# Controlli!
if( Corteccia %>% 
  filter(is.na(diam)) %>% 
  nrow() != 0) stop("ERROR: mancano diametri!")

if( Corteccia %>% 
  filter(is.na(KeyID)) %>% 
  nrow() != 0) stop("ERROR: non aggancia un fusto!")

if( Corteccia %>% 
  select(KeyID, cls_h) %>% 
  unique() %>% 
  right_join(Rotelle) %>% 
  filter(is.na(distSuolo)) %>% 
  nrow() != 0) stop("ERROR: non aggancia una rotella!")

cat("Controlli superati\n")

```

# Transfer measures to the DataBase
```{r transfer}
con <- Open_DB(fm_DB)
tablesList <-  c("Corteccia")
for (tab in tablesList) {
  print(paste("writing table", tab))
  dbSendQuery(con,
            paste("DELETE FROM", tab))
  RSQLite::dbWriteTable(con, tab, get(tab), append = T)
  # Populate the (empty) DB tables (Order of tables has to respect external keys)
  Sys.sleep(1)
}
cat(format(Sys.time(), '%B %d, %Y - %H:%M:%S'))
  Sys.sleep(5)
dbDisconnect(con)

```
